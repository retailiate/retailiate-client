# -*- coding: utf-8 -*-
from frtls.tasks import TaskDesc
from retailiate_client.defaults import RETAILIATE_BASE_TOPIC


class RetailiateTaskDesc(TaskDesc):
    def __init__(self, **kwargs):

        kwargs["topic"] = RETAILIATE_BASE_TOPIC
        # kwargs["open_ended"] = True
        # kwargs["has_childs"] = True
        super().__init__(**kwargs)
