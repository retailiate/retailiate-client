# -*- coding: utf-8 -*-
import csv
import logging
import os
import zipfile
from abc import ABCMeta, abstractmethod
from concurrent.futures.thread import ThreadPoolExecutor
from io import BytesIO
from typing import Any, Dict, Iterable, List, Mapping, Optional

from PIL import Image, ImageColor
from frtls.defaults import DEFAULT_EXCLUDE_DIRS
from frtls.dicts import dict_merge
from frtls.downloads import calculate_cache_location_for_url
from frtls.files import ensure_folder
from frtls.types.typistry import Typistry
from fs import open_fs
from fs.opener.parse import parse_fs_url
from retailiate_client.defaults import retailiate_client_app_dirs
from tings.utils import PathRegexMatcher, file_matches


log = logging.getLogger("retailiate")


class Images(object):
    def __init__(
        self,
        vendor_id: Optional[int],
        ean_mapping: Optional[Mapping[str, List[Dict[str, Any]]]],
        typistry: Typistry = None,
        **image_pack_conf,
    ):

        self._ean_mapping: Optional[Mapping[str, List[Dict[str, Any]]]] = ean_mapping
        self._vendor_id = vendor_id

        if typistry is None:
            typistry = Typistry()

        self._typistry: Typistry = typistry
        self._img_pack_plugins = self._typistry.get_plugin_manager(
            ImagePack, plugin_type="instance"
        )
        self._image_pack_conf = image_pack_conf
        self._image_pack: Optional[ImagePack] = None

        self._product_map: Optional[Mapping[str, Mapping[str, Any]]] = None
        self._invalid_product_map: Optional[List[str]] = None

    @property
    def invalid_product_map(self) -> Iterable[str]:

        if self._invalid_product_map is None:
            self.product_map  # noqa

        return self._invalid_product_map  # type: ignore

    @property
    def product_map(self) -> Mapping[str, Mapping[str, Any]]:

        if self._product_map is not None:
            return self._product_map

        self._product_map = {}
        self._invalid_product_map = []

        for image_id in self.image_pack.get_image_ids():

            if image_id.endswith(".jpg") or image_id.endswith(".png"):
                id = image_id[0:-4]
            elif image_id.endswith(".jpeg"):
                id = image_id[0:-5]
            else:
                continue

            product_id = self.parse_image_id(id)
            if product_id is None:
                self._invalid_product_map.append(image_id)
                continue

            details = self.get_product_details(product_id)
            if details is None:
                self._invalid_product_map.append(image_id)
                continue

            d = dict(details)
            d["product_id"] = product_id
            self._product_map[image_id] = d

        return self._product_map

    def parse_image_id(self, image_id) -> Optional[str]:

        ean = os.path.basename(image_id)
        if "_" in ean:
            ean = ean.split("_")[0]
        if "-" in ean:
            ean = ean.split("-")[0]
        if " " in ean:
            ean = ean.split(" ")[0]
        return ean

    def get_product_details(self, product_id) -> Optional[Mapping[str, Any]]:

        if self._ean_mapping and product_id not in self._ean_mapping.keys():
            return None

        if not self._ean_mapping:
            return {
                "han": product_id,
                "ean": product_id,
                "vendor_id": -1,
                "vendor": "n/a",
            }
        detail_list = self._ean_mapping[product_id]

        details = None
        for d in detail_list:

            if (
                not self._vendor_id and len(detail_list) == 1
            ) or self._vendor_id == int(d["vendor_id"]):
                details = d
                break

            if not self._vendor_id and len(detail_list) > 1:
                log.error(
                    f"Can't determine unique match for id '{product_id}', multiple options: {detail_list}"
                )

        if details is None:
            return None

        return details

    def parse_uri(cls, uri: str) -> Mapping[str, Any]:

        if uri.startswith("ftp"):
            result = parse_fs_url(uri)
            return {
                "type": "ftp",
                "username": result.username,
                "password": result.password,
                "host": result.resource,
            }

        elif uri.endswith(".zip"):
            return {"type": "zip"}
        elif os.path.isdir(uri):
            return {"type": "folder"}
        else:
            raise Exception(f"Can't determine type of image pack with uri: {uri}")

    @property
    def image_pack(self):

        if self._image_pack is not None:
            return self._image_pack

        temp = dict(self._image_pack_conf)
        img_pack_type = temp.pop("type", None)

        if img_pack_type is None:

            if "uri" not in temp.keys():
                raise Exception(
                    f"Can't determine type of image pack: {self._image_pack_conf}"
                )

            uri = temp["uri"]
            _t = dict(self.parse_uri(uri))
            config = dict_merge(_t, temp, copy_dct=True)
            img_pack_type = config.pop("type")
        else:
            config = temp

        plugin = self._img_pack_plugins.get_plugin(img_pack_type)
        self._image_pack = plugin(**config)
        return self._image_pack

    def export(
        self,
        target_path: Optional[str] = None,
        path_prefix: Optional[str] = None,
        quality: int = 75,
        parallel: Optional[int] = None,
        resize_width: int = 1280,
    ) -> str:

        if target_path is None:
            base_name = self.image_pack.get_pack_name()
            target_path = os.path.join(os.getcwd(), base_name)

        target_path = os.path.abspath(os.path.expanduser(target_path))

        ensure_folder(os.path.join(target_path, "images"))

        result = [
            [
                "EAN",
                "Bild Pfad/URL 1",
                "Bild Pfad/URL 2",
                "Bild Pfad/URL 3",
                "Bild Pfad/URL 4",
                "Bild Pfad/URL 5" "Bild Pfad/URL 6",
            ]
        ]

        def wrap(_id, _img, _target_file, _quality, _resize_width):
            log.debug(f"writing: {target_file}")
            try:
                log.info(f"exporting: {_id}")
                self.write_resized_image(
                    img=_img,
                    path=_target_file,
                    quality=_quality,
                    resize_width=_resize_width,
                )
            except Exception as e:
                log.error(f"Can't convert file '{_id}': {e}")
                log.debug("Error converting file.", exc_info=True)

        use_threads = parallel != 1
        if use_threads:
            executor: Optional[ThreadPoolExecutor] = ThreadPoolExecutor(
                max_workers=parallel
            )
        else:
            executor = None

        ean_map: Dict[str, List[Mapping[str, Any]]] = {}
        for id, details in self.product_map.items():

            ean = details["ean"]
            ean_map.setdefault(ean, []).append({"id": id, "details": details})

        for ean, data in ean_map.items():

            temp = [ean]
            for index, image_details in enumerate(data):

                # details = image_details["details"]
                id = image_details["id"]

                target_file_name = f"{ean}_{index}.jpg"
                target_file = os.path.join(target_path, "images", target_file_name)
                log.debug(f"submit job for: {id}")

                img = self.image_pack.get_product_image(id)

                if use_threads:
                    executor.submit(  # type: ignore
                        wrap, id, img, target_file, quality, resize_width
                    )  # type: ignore
                else:
                    wrap(id, img, target_file, quality, resize_width)

                if path_prefix is None:
                    _p = "\\".join(["images", target_file_name])
                else:
                    _p = "\\".join([path_prefix, "images", target_file_name])
                temp.append(_p)
            result.append(temp)

        if use_threads:
            log.debug("wating for all tasks to finish...")
            executor.shutdown(wait=True)  # type: ignore
            log.debug("tasks finished")

        csv_path = os.path.join(target_path, "image_list.csv")

        with open(csv_path, "w") as csvfile:
            filewriter = csv.writer(csvfile, delimiter=";", quoting=csv.QUOTE_NONE)
            for line in result:
                filewriter.writerow(line)

        return target_path

    def write_resized_image(
        self,
        img: Image,
        path: str,
        quality: int = 75,
        resize_width: int = 1280,
        fill_color="#ffffff",
    ):

        width, height = img.size
        if width > resize_width:
            wpercent = resize_width / float(width)
            hsize = int((float(height) * float(wpercent)))
            img = img.resize((resize_width, hsize), Image.LANCZOS)

        if img.mode in ("RGBA", "LA"):
            log.debug(f"converting png to jpeg: {path}")
            col = ImageColor.getcolor(fill_color, img.mode[:-1])
            background = Image.new(img.mode[:-1], img.size, col)
            background.paste(img, img.split()[-1])
            img = background

        img.save(path, img.format, quality=quality, optimize=True, progressive=True)


class ImagePack(metaclass=ABCMeta):
    def __init__(self, pack_name: Optional[str] = None, **kwargs):

        self._pack_name = pack_name
        self._cache_location = os.path.join(
            retailiate_client_app_dirs.user_cache_dir,
            "image_packs",
            self.__class__.__name__,
        )

    @abstractmethod
    def get_image_ids(self) -> Iterable[str]:
        pass

    @abstractmethod
    def get_product_image(self, image_id: str) -> Image:
        pass

    def get_pack_name(self) -> str:

        if self._pack_name is not None:
            return self._pack_name

        return self._get_pack_name()

    @abstractmethod
    def _get_pack_name(self) -> str:
        pass


class FolderImagePack(ImagePack):

    _plugin_name = "folder"

    def __init__(self, uri, **kwargs):

        self._path = os.path.abspath(uri)
        self._image_names: Optional[List[str]] = None
        self._search_exclude_dirs = DEFAULT_EXCLUDE_DIRS
        self._matchers = [PathRegexMatcher(regex=r".*\.(jpg|jpeg|png)$")]

        super().__init__(**kwargs)

    def _get_pack_name(self) -> str:

        return os.path.basename(os.path.splitext(self._path)[0])

    def get_product_image(self, image_id: str) -> Image:

        img = Image.open(image_id)

        return img

    def get_image_ids(self) -> Iterable[str]:

        if self._image_names is not None:
            return self._image_names

        self._image_names = []

        for root, dirnames, filenames in os.walk(self._path, topdown=True):

            if self._search_exclude_dirs:
                dirnames[:] = [
                    d for d in dirnames if d not in self._search_exclude_dirs
                ]

            for filename in [
                f
                for f in filenames
                if file_matches(path=os.path.join(root, f), matcher_list=self._matchers)
            ]:

                path = os.path.join(root, filename)

                self._image_names.append(path)

        return self._image_names


class ZipImagePack(ImagePack):

    _plugin_name = "zip"

    def __init__(self, uri, **kwargs):

        self._path = uri
        self._zip_file: Optional[zipfile.ZipFile] = None

        super().__init__(**kwargs)

    def _get_pack_name(self) -> str:

        return os.path.basename(os.path.splitext(self._path)[0])

    def get_product_image(self, image_id: str) -> Image:

        # entry = self.zip_file.getinfo(path)

        image_data = self.zip_file.read(image_id)
        fh = BytesIO(image_data)
        img = Image.open(fh)
        # with self.zip_file.open(entry) as file:
        #     img: Image = Image.open(file)
        #     # print(img.size, img.mode, len(img.getdata()))

        return img

    @property
    def zip_file(self) -> zipfile.ZipFile:

        if self._zip_file is None:
            self._zip_file = zipfile.ZipFile(self._path)
        return self._zip_file

    def get_image_ids(self) -> Iterable[str]:

        return self.zip_file.namelist()


class FtpImagePack(ImagePack):

    _plugin_name = "ftp"

    def __init__(
        self,
        host: str,
        username: Optional[str] = None,
        password: Optional[str] = None,
        **kwargs,
    ):

        self._protocol = "ftp"
        self._host = host
        self._username = username
        self._password = password

        temp = f"{self._protocol}://"
        if self._username:
            temp = f"{temp}{self._username}"
            if self._password:
                temp = f"{temp}:{self._password}"

            temp = f"{temp}@"

        self._url = f"{temp}{self._host}"

        self._filesystem = None
        self._image_names: Optional[List[str]] = None

        super().__init__(**kwargs)

    @property
    def filesystem(self):

        if self._filesystem is None:
            self._filesystem = open_fs(self._url)
        return self._filesystem

    def _get_pack_name(self) -> str:

        return self._url.split("@")[-1]

    def get_product_image(self, image_id: str) -> Image:

        full = f"{self._host}/{image_id}"
        cache_path = calculate_cache_location_for_url(
            full, repl_chars="[^_\\-A-Za-z0-9.\\s]+"
        )

        cache_full = os.path.join(self._cache_location, cache_path)
        if os.path.exists(cache_full):
            last_modified = os.path.getmtime(cache_full)
        else:
            last_modified = 0

        info = self.filesystem.getinfo(image_id, namespaces=["details"])
        remote_modified = info.modified

        if last_modified < remote_modified.timestamp():
            log.info(f"no cache for image '{image_id}', downloading...")
            ensure_folder(os.path.dirname(cache_full))
            with open(cache_full, "wb") as f:
                self.filesystem.download(image_id, f)
        else:
            log.info(f"cache exists for image '{image_id}', not downloading again")

        img = Image.open(cache_full)
        return img

    def get_image_ids(self) -> Iterable[str]:

        if self._image_names is not None:
            return self._image_names

        self._image_names = []
        log.info(f"Retrieving directory listing for: {self._host}")
        for path in self.filesystem.walk.files(filter=["*.jpeg", "*.jpg", "*.png"]):
            self._image_names.append(path)

        return self._image_names
