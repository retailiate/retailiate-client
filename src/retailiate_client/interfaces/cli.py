# -*- coding: utf-8 -*-
import logging
from typing import Any, List, Mapping, Union

import asyncclick as click
from bring.defaults import BRING_TASKS_BASE_TOPIC
from freckworks.defaults import FRECKWORKS_BASE_TOPIC
from freckworks.interfaces.cli.apply import FreckworksProjectApplyCommand
from freckworks.interfaces.cli.info import FreckworksProjectInfoCommand
from frtls.cli.exceptions import handle_exc_async
from frtls.cli.logging import logzero_option_async
from frtls.cli.self_command_group import self_command
from frtls.introspection.pkg_env import AppEnvironment
from frtls.tasks.task_watcher import TaskWatchManager
from retailiate_client.interfaces.images import images
from retailiate_client.retailiate import Retailiate


log = logging.getLogger("retailiate-client")

click.anyio_backend = "asyncio"

try:
    import uvloop

    uvloop.install()
except Exception:
    pass


retailiate: Retailiate = Retailiate()


@click.group()
@click.option(
    "--task-log",
    multiple=True,
    required=False,
    type=str,
    help=f"output plugin(s) for running tasks. available: {', '.join(['terminal', 'simple'])}",
)
@logzero_option_async(default_verbosity="INFO")
@click.pass_context
@handle_exc_async
async def cli(ctx, task_log):

    ctx.obj = {}
    ae = AppEnvironment()
    log.debug(f"build_time: {ae.build_time}")

    watchers: List[Union[str, Mapping[str, Any]]] = []
    for to in task_log:
        watchers.append(
            {
                "type": to,
                "base_topics": [BRING_TASKS_BASE_TOPIC, FRECKWORKS_BASE_TOPIC],
                "terminal": retailiate.terminal,
            }
        )

    watch_mgmt = TaskWatchManager(
        typistry=retailiate._tingistry.typistry, watchers=watchers
    )

    ctx.obj["watch_mgmt"] = watch_mgmt


info_cmd = FreckworksProjectInfoCommand(
    name="info", project=retailiate.project, terminal=retailiate.terminal
)
cli.add_command(info_cmd)
apply_cmd = FreckworksProjectApplyCommand(
    name="apply", project=retailiate.project, terminal=retailiate.terminal
)
cli.add_command(apply_cmd)

cli.add_command(images)
cli.add_command(self_command)

if __name__ == "__main__":
    cli()
