# -*- coding: utf-8 -*-
import csv
import logging
from typing import Any, Dict, List, Mapping, Optional

import asyncclick as click
from frtls.cli.exceptions import handle_exc_async
from frtls.downloads import download_cached_binary_file_async
from retailiate_client.pim.images import Images
from tabulate import tabulate


log = logging.getLogger("retailiate")


@click.group()
@click.pass_context
@handle_exc_async
async def images(ctx):
    """import/export product impage packs"""

    pass


def read_mapping_file(mapping_file) -> Mapping[str, List[Dict[str, Any]]]:

    mapping_dict: Dict[str, List[Dict[str, Any]]] = {}

    log.info(f"Importing: {mapping_file}...")
    with open(mapping_file) as f:
        csv_reader = csv.reader(f, delimiter=";", quotechar='"')
        i = 0
        for line in csv_reader:
            if i == 0:
                i = i + 1
                continue
            else:
                i = i + 1
                han = line[0]
                if len(line) < 4:
                    log.error(f"Invalid number of columns, ignoring: {line}")
                    continue
                data = {
                    "han": han,
                    "ean": line[1],
                    "vendor_id": line[2],
                    "vendor": line[3],
                }
                mapping_dict.setdefault(han, []).append(data)

    return mapping_dict


@images.command()
@click.argument("path_or_url", nargs=1, type=str)
@click.option(
    "--update", "-u", is_flag=True, help="updated cached download", default=False
)
@click.option("--vendor-id", "-v", help="the vendor id", required=False, type=int)
@click.option("--mapping-file", "-m", help="ean/han mapping file", required=False)
@click.pass_context
@handle_exc_async
async def info(ctx, path_or_url: str, vendor_id: int, mapping_file: str, update: bool):
    """Display info about a product image archive"""

    if not path_or_url.startswith("http"):
        _uri = path_or_url
    else:
        _uri = await download_cached_binary_file_async(
            url=path_or_url, update=update
        )  # type: ignore

    if mapping_file:
        mapping_dict: Optional[Mapping[str, List[Dict[str, Any]]]] = read_mapping_file(
            mapping_file
        )
    else:
        mapping_dict = None

    images_obj = Images(vendor_id=vendor_id, ean_mapping=mapping_dict, uri=_uri)

    ean_map = images_obj.product_map

    data = []
    for k, v in ean_map.items():
        ean = v["ean"]
        id = v["product_id"]
        vendor_id = v["vendor_id"]
        vendor = v["vendor"]
        data.append([k, id, ean, vendor, vendor_id])

    click.echo(
        tabulate(
            data,
            headers=["image_name", "id", "ean", "vendor", "vendor_id"],
            tablefmt="presto",
        )
    )

    if images_obj.invalid_product_map:
        click.echo()
        click.echo("Invalid items:")
        click.echo()
        for i in images_obj.invalid_product_map:
            click.echo(f"  - {i}")
        click.echo()


@images.command()
@click.argument("path_or_url", nargs=1, type=str)
@click.option(
    "--target",
    "-t",
    help="target directory",
    type=click.Path(dir_okay=True, file_okay=False),
    required=False,
)
@click.option("--vendor-id", "-v", help="the vendor id", required=False, type=int)
@click.option(
    "--prefix", "-p", help="export prefix (for csv)", type=str, required=False
)
@click.option(
    "--update", "-u", is_flag=True, help="updated cached download", default=False
)
@click.option(
    "--quality", "-q", type=click.IntRange(0, 100), help="jpeg quality", default=75
)
@click.option(
    "--parallel",
    "-p",
    type=int,
    help="number of parallel image processing threads, defaults to number of cores",
    required=False,
)
@click.option("--mapping-file", "-m", help="ean/han mapping file", required=True)
@click.pass_context
@handle_exc_async
async def export(
    ctx,
    path_or_url: str,
    vendor_id: int,
    mapping_file: str,
    quality: int = 75,
    prefix: Optional[str] = None,
    target: Optional[str] = None,
    update: bool = False,
    parallel: Optional[int] = None,
):
    """Export image pack."""

    if not path_or_url.startswith("http"):
        _uri = path_or_url
    else:
        _uri = await download_cached_binary_file_async(  # type: ignore
            url=path_or_url, update=update
        )  # type: ignore

    mapping_dict = read_mapping_file(mapping_file)

    images_obj = Images(vendor_id=vendor_id, ean_mapping=mapping_dict, uri=_uri)

    click.echo()
    click.echo("exporting...")

    target = images_obj.export(
        target_path=target, path_prefix=prefix, quality=quality, parallel=parallel
    )

    click.echo(f"ImagePack exported to: {target}")
