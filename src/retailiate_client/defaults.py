# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs


retailiate_client_app_dirs = AppDirs("retailiate_client", "frkl")

if not hasattr(sys, "frozen"):
    RETAILIATE_CLIENT_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `retailiate_client` module."""
else:
    RETAILIATE_CLIENT_MODULE_BASE_FOLDER = os.path.join(
        sys._MEIPASS, "retailiate_client"  # type: ignore
    )
    """Marker to indicate the base folder for the `retailiate_client` module."""

RETAILIATE_CLIENT_RESOURCES_FOLDER = os.path.join(
    RETAILIATE_CLIENT_MODULE_BASE_FOLDER, "resources"
)

RETAILIATE_PROJECT_CONFIG = os.path.join(RETAILIATE_CLIENT_RESOURCES_FOLDER, "config")

RETAILIATE_BIN_FOLDER = os.path.join(retailiate_client_app_dirs.user_data_dir, "bin")

RETAILIATE_BASE_TOPIC = "retailiate.tasks"

RETAILIATE_PRELOAD = ["retailiate_client.retailiate"]
