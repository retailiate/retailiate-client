# -*- coding: utf-8 -*-

"""Main module."""
import os
from pathlib import Path
from typing import Any, Iterable, Mapping, Optional, Union

from blessed import Terminal
from freckworks.core.config import FolderFreckworksProjectConfig
from freckworks.core.freckworks import FreckworksProject
from freckworks.defaults import FRECKWORKS_PRELOAD_MODULES
from frtls.async_helpers import wrap_async_task
from frtls.cli.terminal import create_terminal
from frtls.types.utils import load_modules
from retailiate_client.defaults import RETAILIATE_PROJECT_CONFIG
from tings.tingistry import Tingistries


class RetailiateConfig(FolderFreckworksProjectConfig):
    def __init__(
        self,
        name: str,
        meta: Optional[Mapping[str, Any]] = None,
        terminal: Terminal = None,
    ):

        super().__init__(name=name, meta=meta, terminal=terminal)
        self.input.set_values(project_folder=RETAILIATE_PROJECT_CONFIG)


class RetailiateProject(FreckworksProject):
    def __init__(
        self,
        name: str,
        meta: Optional[Mapping[str, Any]] = None,
        init_frecktings: Optional[Iterable[Union[str, Mapping[str, Any]]]] = None,
        init_values: Optional[Mapping[str, Any]] = None,
        terminal: Terminal = None,
    ):

        # init_frecktings = ["kube", "argocd"]
        # init_values = {"cluster_name": "retailiate"}
        super().__init__(
            name=name,
            meta=meta,
            init_frecktings=init_frecktings,
            init_values=init_values,
            terminal=terminal,
        )


PROFILES = {
    "k3d": {
        "frecktings": ["k3d"],
        "values": {
            "cluster_name": "retailiate",
            "k3d_version": "1.7.0",
            "kubectl_version": "1.17.2",
            "argocd_namespace": "argocd",
        },
    }
}


class Retailiate(object):
    def __init__(self, project_folder: Optional[Union[str, Path]] = None):

        if project_folder is None:
            project_folder = os.getcwd()

        if isinstance(project_folder, Path):
            project_folder = project_folder.resolve().as_posix()

        self._project_folder = project_folder
        self._project_config = os.path.join(self._project_folder, "retailiate.yaml")
        self._manifest_folder = os.path.join(self._project_folder, "manifests")
        self._util_path = os.path.join(self._project_folder, ".utils")
        self._kubectl_path = os.path.join(self._project_folder, ".utils")

        load_modules(FRECKWORKS_PRELOAD_MODULES)
        self._terminal = create_terminal()

        self._tingistry = Tingistries.create("retailiate")
        self._tingistry.register_prototing(
            "freckworks.proto.project_config", RetailiateConfig, terminal=self._terminal
        )

        self._freckworks_project_config: RetailiateConfig = self._tingistry.create_ting(
            "freckworks.proto.project_config", "freckworks.projects.default_project"
        )
        self._freckworks_project: Optional[FreckworksProject] = None

        self._profile_name = "k3d"

    def create_project(self):

        project_details = PROFILES[self._profile_name]

        project = wrap_async_task(
            self._freckworks_project_config.get_value, "project"
        )  # type: ignore

        frecktings = project_details.get("frecktings", [])
        values: Mapping[str, Any] = project_details.get("values", None)
        if values is None:
            values = {}
        else:
            values = dict(values)

        # values["manifest_folder"] = self._manifest_folder
        values["k3d_path"] = self._util_path
        values["kubectl_path"] = self._util_path
        values["retailiate_mgmt_pkg_target"] = os.path.join(
            self._manifest_folder, "mgmt"
        )

        for ft in frecktings:
            project.register_freckting(ft)

        project.set_values(**values)

        return project

    @property
    def project(self) -> FreckworksProject:

        if self._freckworks_project is None:
            self._freckworks_project = self.create_project()
        return self._freckworks_project

    @property
    def terminal(self) -> Terminal:

        return self._terminal
