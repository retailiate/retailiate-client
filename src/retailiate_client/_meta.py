# -*- coding: utf-8 -*-
app_name = "retailiate"
freckops_default_cluster_name = "retailiate"

pyinstaller = {
    "hiddenimports": [
        "retailiate_client.defaults",
        "packaging.requirements",
        "pkg_resources.py2_warn",
    ]
}
