[![PyPI status](https://img.shields.io/pypi/status/retailiate-client.svg)](https://pypi.python.org/pypi/retailiate-client/)
[![PyPI version](https://img.shields.io/pypi/v/retailiate-client.svg)](https://pypi.python.org/pypi/retailiate-client/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/retailiate-client.svg)](https://pypi.python.org/pypi/retailiate-client/)
[![Pipeline status](https://gitlab.com/frkl/retailiate-client/badges/develop/pipeline.svg)](https://gitlab.com/frkl/retailiate-client/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# retailiate-client

*Client application for retailiate*

# Installation

Download url, then make executable. E.g.:

```
wget https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/linux-gnu/retailiate
chmod +x retailiate
```

## Linux
 - dev: https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/linux-gnu/retailiate
 - stable: TBD

## Windows

 - dev: https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/windows/retailiate.exe

## Mac OS X

TBD

## Description

Documentation still to be done.

## Usage

### Update

```
❯ retailiate self update
```

### Get help

```
❯ retailiate images --help
Usage: retailiate images [OPTIONS] COMMAND [ARGS]...

  import/export product impage packs

Options:
  --help  Show this message and exit.

Commands:
  export  Export image pack.
  info    Display info about a product image archive
```

Or, get help for any of the sub-commands:
```
❯ retailiate images export --help
Usage: retailiate images export [OPTIONS] ZIPFILE

  Export image pack.

Options:
  -t, --target DIRECTORY  target directory
  -p, --prefix TEXT       export prefix (for csv)
  -u, --update            updated cached download
  --help                  Show this message and exit.
```

### ImagePack info

Retrieve info for ImagePack:

```
❯ retailiate images info PACKSHOTS_CURRENT_ASSORTMENT-72DPI.zip
 image_name                               |   guessed ean
------------------------------------------+---------------
 wild_life/packshots/42351.jpg            |         42351
 wild_life/packshots/42353.jpg            |         42353
 wild_life/packshots/42410.jpg            |         42410
...
...
...
```

Manually check ean is correct.

### Export ImagePack

```
# export to current directory, relative paths in csv
❯ retailiate images export PACKSHOTS_CURRENT_ASSORTMENT-72DPI.zip

# or, write to specific target dir and include (Windows path) prefix in csv
❯ retailiate images export --target /tmp/products --prefix "V:\Onlineshop\Lieferantendaten" CURRENT_ASSORTMENT-72DPI.zip

```

# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'retailiate-client' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 retailiate_client
    git clone https://gitlab.com/frkl/retailiate_client
    cd <retailiate_client_dir>
    pyenv local retailiate_client
    pip install -e .[all-dev]
    pre-commit install


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!).

[The Prosperity Public License 3.0.0](https://licensezero.com/licenses/prosperity)

[Copyright (c) 2020 frkl OÜ](https://frkl.io)
